""" Employee Admin. """

# Django
from django.contrib import admin
from django.utils.html import mark_safe

# Local
from galicia_db.employees.models import Employee

@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    readonly_fields = ["ine_image"]
    list_display = ["first_name", "last_name", "personal_phone", "emergency_phone"]
    search_fields = ["first_name", "last_name"]
    
    def ine_image(self, obj):
        return mark_safe(
            '<img src="{url}" width="5vvw", height="2vh" />'.format(
                url = obj.ine.url
            )
        )