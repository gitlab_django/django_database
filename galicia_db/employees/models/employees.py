""" Employees is the main object model. """

# Django
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator

# Utils
from galicia_db.utils.abstract_models import CreateUpdateModel

class Employee(CreateUpdateModel):
    """ Employee is the main object, this has the fields to control,
        the important data of the galicia's employees."""

    first_name = models.CharField(_("Name"), max_length=50)
    last_name = models.CharField(_("Last Name"), max_length=50)
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{10,12}',
        message="Phone number must be entered in 10 digits format."
    )
    personal_phone = models.CharField(
        _("Personal phone"),
        validators=[phone_regex],
        max_length=50,
        blank=False
    )
    emergency_phone = models.CharField(
        _("Emergency Phone"),
        validators=[phone_regex],
        max_length=50,
        blank=False
    )
    address = models.CharField(
        max_length=255,
        blank=False
    )
    ine = models.ImageField(
        _("INE picture"), 
        upload_to='employee',
        blank=True,
        null=True 
    )
    allergies = models.TextField(_("Allergies"))
    type_bloods = [
        ("O-", "O-"),
        ("O+", "O+"),
        ("A-", "A-"),
        ("A+", "A+"),
        ("B+", "B+"),
        ("B-", "B-"),
        ("AB+", "AB+"),
        ("AB-", "AB-"),
        ("NE", "NE")
    ]
    kind_blood = models.CharField(
        _("Kind of blood"), 
        max_length=4,
        choices=type_bloods,
        default="NE"
    )

    class Meta:
        verbose_name = "Empleado"
        verbose_name_plural = "Empleados de Galicia"

    def __str__(self):
        return self.first_name + '-' + self.last_name
    