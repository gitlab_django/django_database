from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class EmployeesConfig(AppConfig):
    name = "galicia_db.employees"
    verbose_name = _("Employees")

    def ready(self):
        try:
            import galicia_db.employees.signals  # noqa F401
        except ImportError:
            pass
