from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.db import models

# Utils
from galicia_db.utils.abstract_models import CreateUpdateModel


class User(AbstractUser, CreateUpdateModel):
    """Default user for Galicia Base de datos."""

    #: First and last name do not cover name patterns around the globe
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})
